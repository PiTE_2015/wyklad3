__author__ = 'ja'

import abc
import  os

class NotSuchFile(Exception):
    pass

class NotAString(Exception):
    pass

class AbstractValidator(object):
    __metaclass__  = abc.ABCMeta
    @abc.abstractmethod
    def validate(self,filename):
        """draw the enemy in the game area"""

class OptionValidator(AbstractValidator):

    def __init__(self, filename):
        self.filename=filename

    def validate(self):
        if not self._isString():
            raise NotAString()
        if not self._file_exist():
            raise NotSuchFile()

    def _isString(self):
        return isinstance(self.filename,str)

    def _file_exist(self):
        return os.path.isfile(self.filename)

class InputFileValidator(AbstractValidator):
	pass
