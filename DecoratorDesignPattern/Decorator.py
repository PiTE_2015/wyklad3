__author__ = 'ja'

class AbstractArmor(object):
    def __init__(self):
        self.defence = 0
        self.durability = 0
        self.name="DefaultArmor"

    def print_attributes(self):
        print("My name is "+self.name)
        print("My defence is "+str(self.defence))
        print("My durability is "+str(self.durability))


class Decorator(AbstractArmor):
    def __init__(self, decorated,new_name):
        pass

class UpgradeDecorator(Decorator):
    def __init__(self, decorated,new_name):
        increase_defence = 10
        increase_durability = 2
        self.defence = decorated.defence+increase_defence
        self.durability=decorated.durability+increase_durability
        self.name=new_name

class BearArmour(AbstractArmor):
    def __init__(self):
        self.defence = 5
        self.durability = 1
        self.name="BearArmor"

def main():
    armor = BearArmour()
    armor.print_attributes()
    print("first decoration")
    armor=UpgradeDecorator(armor, "Enhanced Bear Armor")
    armor.print_attributes()
    print("second decoration")
    armor=UpgradeDecorator(armor, "Mastercrafted Bear Armor")
    armor.print_attributes()


if __name__ == "__main__":
    main()