__author__ = 'ja'

import abc
import pygame
import sys
import NecrophageFactory


class I_EventHandler(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def handleEvent(self):
        """method for handling the pygame evet"""


class QuitHandler(I_EventHandler):
    def handleEvent(self):
        pygame.quit()
        print "end of the game"
        sys.exit()


class MouseClickHandler(I_EventHandler):
    def __init__(self, gameDisplay, necrophageType, enemyList):
        self.necrophageFactory = NecrophageFactory.NecrophageFactory(gameDisplay)
        self.necrophageType = necrophageType
        self.enemyList = enemyList

    def handleEvent(self):
        try:
            positionX, positionY = pygame.mouse.get_pos()
            newEnemy = self.necrophageFactory.createEnemy(self.necrophageType[0], positionX, positionY)
            self.enemyList.append(newEnemy)
        except NecrophageFactory.NoSuchEnemy, ex:
            print("state problem:" + ex.message)


class KeyDownHandler(I_EventHandler):
    def __init__(self, necrophageType):
        self.necrophageType = necrophageType

    def handleEvent(self):
        pressedKeys = pygame.key.get_pressed()
        if pressedKeys[pygame.K_1]:
            self.necrophageType[0] = NecrophageFactory.NecrophageTypes.Ghoul
        elif pressedKeys[pygame.K_2]:
            self.necrophageType[0] = NecrophageFactory.NecrophageTypes.Alghoul
        elif pressedKeys[pygame.K_3]:
            self.necrophageType[0] = NecrophageFactory.NecrophageTypes.Drowner
        elif pressedKeys[pygame.K_4]:
            self.necrophageType[0] = NecrophageFactory.NecrophageTypes.Cemetaur
