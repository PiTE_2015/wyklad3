__author__ = 'ja'

import abc
import pygame
import Colors

class I_Necrophage(object):
    __metaclass__  = abc.ABCMeta
    @abc.abstractmethod
    def draw(self,positionX,positionY):
        """draw the enemy in the game area"""

class Necrophage(I_Necrophage):
    def __init__(self,gameDisplay,positionX,positionY):
        self.positionX=positionX
        self.positionY=positionY
        self.gameDisplay=gameDisplay
        self.color=Colors.Colors.white
        self.width=0
        self.height=0

    def draw(self):
        pygame.draw.rect(self.gameDisplay, self.color, [self.positionX, self.positionY, self.width, self.height])

class Ghoul(Necrophage):
    def __init__(self,gameDisplay,positionX,positionY):
        super(Ghoul,self).__init__(gameDisplay,positionX,positionY)
        self.color=Colors.Colors.green
        self.width=15
        self.height=15


class Alghoul(Necrophage):
    def __init__(self,gameDisplay,positionX,positionY):
        super(Alghoul,self).__init__(gameDisplay,positionX,positionY)
        self.color=Colors.Colors.pink
        self.width=40
        self.height=40

class Drowner(Necrophage):
    def __init__(self,gameDisplay,positionX,positionY):
        super(Drowner,self).__init__(gameDisplay,positionX,positionY)
        self.color=Colors.Colors.darkBlue
        self.width=60
        self.height=60

class Cemetaur(Necrophage):
    def __init__(self,gameDisplay,positionX,positionY):
        super(Cemetaur,self).__init__(gameDisplay,positionX,positionY)
        self.color=Colors.Colors.red
        self.width=100
        self.height=100
