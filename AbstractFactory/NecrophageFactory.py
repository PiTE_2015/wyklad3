__author__ = 'ja'

import exceptions
import Necrophage
import enum


class NecrophageTypes(enum.Enum):
    Ghoul =1
    Alghoul = 2
    Drowner =3
    Cemetaur = 4

class NoSuchEnemy(exceptions.Exception):
    def __init__(self, message):
        super(NoSuchEnemy, self).__init__(message)

class NecrophageFactory(object):
    def __init__(self,gameDisplay):
        self.gameDisplay=gameDisplay

    def createEnemy(self, enemyType,positionX,positionY):
        if enemyType == NecrophageTypes.Ghoul:
            return Necrophage.Ghoul(self.gameDisplay,positionX,positionY)
        elif enemyType == NecrophageTypes.Alghoul:
            return Necrophage.Alghoul(self.gameDisplay,positionX,positionY)
        elif enemyType == NecrophageTypes.Drowner:
            return Necrophage.Drowner(self.gameDisplay,positionX,positionY)
        elif enemyType == NecrophageTypes.Cemetaur:
            return Necrophage.Cemetaur(self.gameDisplay,positionX,positionY)
        else:
            raise NoSuchEnemy(enemyType)


