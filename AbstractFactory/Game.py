__author__ = 'ja'

import time
import random
import pygame

import Colors
import EventHandlers
import NecrophageFactory

class Game:

    def __init__(self):
        pygame.init()
        self.necrophageType=[NecrophageFactory.NecrophageTypes.Ghoul] # workaround due to python enums are immutable.
        self._setupGameParameters()
        self.enemyList=[]
        self._createEventHandlers()

    def _setupGameParameters(self):
        self.display_width = 800
        self.display_height = 600
        self.gameDisplay = pygame.display.set_mode((self.display_width, self.display_height))
        pygame.display.set_caption('Abstract factory design pattern demonstration')
        self.clock = pygame.time.Clock()
        self.color = Colors.Colors.white

    def _createEventHandlers(self):
        self.eventHandlers={
            pygame.MOUSEBUTTONUP:EventHandlers.MouseClickHandler(self.gameDisplay,self.necrophageType,self.enemyList),
            pygame.QUIT:EventHandlers.QuitHandler(),
            pygame.KEYDOWN: EventHandlers.KeyDownHandler(self.necrophageType)
        }

    def _game_loop(self):
        print "start game"
        while True:
            for event in pygame.event.get():
                if event.type in self.eventHandlers.keys():
                    self.eventHandlers[event.type].handleEvent()
            self._displayAll()

    def _displayAll(self):
        self.gameDisplay.fill(self.color)
        self._displayEnemies()
        self._displayMessage("currently create: " +self.necrophageType[0].name)
        pygame.display.update()
        self.clock.tick(60)

    def _displayMessage(self,text):
        textFont = pygame.font.Font('freesansbold.ttf',24)
        textSurface= textFont.render(text,True,Colors.Colors.black )
        textRectangle=textSurface.get_rect()
        textRectangle.center = ((self.display_width-200),(20))
        self.gameDisplay.blit(textSurface, textRectangle)

    def _displayEnemies(self):
        for enemy in self.enemyList:
            enemy.draw()

    def startGame(self):
        self._game_loop()
        pygame.quit()
        quit()

if __name__ == "__main__":
    game=Game()
    game.startGame()