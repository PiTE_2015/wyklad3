from unittest import TestCase
import EventHandlers
import mock
import pygame

__author__ = 'ja'

class TestQuitHandler(TestCase):
    def setUp(self):
        self.handler=EventHandlers.QuitHandler()

    @mock.patch('pygame.quit',return_value=None)
    @mock.patch('sys.exit',return_value=0)
    def test_handleEvent(self,mock_pygameQuit,mock_systemExit):
        self.handler.handleEvent()


class MouseClickHandler(TestCase):
    def setUp(self):
        self.handler=EventHandlers.MouseClickHandler()

    @mock.patch('pygame.mouse.get_pos',return_value=(1,2))
    def test_handleEvent(self,mock_method):
        self.handler.handleEvent()
