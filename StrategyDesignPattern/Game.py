__author__ = 'ja'

import time
import random
import pygame

import Colors
import EventHandlers
import Witcher

class Game:

    def __init__(self):
        pygame.init()
        self.display_width = 800
        self.display_height = 600
        self.gameDisplay = pygame.display.set_mode((self.display_width, self.display_height))
        pygame.display.set_caption('Strategy design pattern demonstration')
        self.clock = pygame.time.Clock()
        self.color=Colors.Colors.white
        self.witcher=Witcher.Witcher(self.gameDisplay)
        self._createEventHandlers()

    def _createEventHandlers(self):
        self.eventHandlers={
            pygame.QUIT:EventHandlers.QuitHandler(),
            pygame.MOUSEBUTTONUP:EventHandlers.MouseClickHandler(self.witcher),
            pygame.KEYDOWN: EventHandlers.KeyDownHandler(self.witcher)
        }

    def _game_loop(self):
        print "start game"
        while True:
            for event in pygame.event.get():
               if event.type in self.eventHandlers.keys():
                   self.eventHandlers[event.type].handleEvent()
            self._displayAll()

    def _displayAll(self):
        self.gameDisplay.fill(self.color)
        self.witcher.draw()
        pygame.display.update()
        self.clock.tick(60)


    def startGame(self):
        self._game_loop()
        pygame.quit()
        quit()


if __name__ == "__main__":
    game=Game()
    game.startGame()