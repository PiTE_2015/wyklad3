__author__ = 'ja'

import  abc
import math
import Colors

class I_MoveBahavoir(object):
     __metaclass__  = abc.ABCMeta

     @abc.abstractmethod
     def makeMove(self,witcher):
         """this method is called when you want to move the witcher"""


class Walk(I_MoveBahavoir):

    def makeMove(self,witcher):
        self._setWitcherAtributes(witcher)
        self._moveIt(witcher)

    def _moveIt(self,witcher):
        if self._notNeedToMakeMove(witcher):
                return  None
        self._balanceMove(witcher)
        self._makeSingleMove(witcher)


    def _balanceMove(self, witcher):
        if (math.fabs(witcher.positionX - witcher.nextPositionX) < witcher.speed):
            witcher.positionX = witcher.nextPositionX
        if (math.fabs(witcher.positionY - witcher.nextPositionY) < witcher.speed):
            witcher.positionY = witcher.nextPositionY

    def _setWitcherAtributes(self,witcher):
        witcher.color=Colors.Colors.black
        witcher.width=20

    def _notNeedToMakeMove(self,witcher):
        return((witcher.positionX,witcher.positionY)==(witcher.nextPositionX,witcher.nextPositionY))

    def _makeSingleMove(self, witcher):
        if (witcher.positionX < witcher.nextPositionX):
            witcher.positionX += witcher.speed
        elif (witcher.positionX > witcher.nextPositionX):
            witcher.positionX -= witcher.speed
        if (witcher.positionY < witcher.nextPositionY):
            witcher.positionY += witcher.speed
        elif (witcher.positionY > witcher.nextPositionY):
            witcher.positionY -= witcher.speed


class Swimm(Walk):

    def makeMove(self,witcher):
        witcher.speed=1
        witcher.color=Colors.Colors.blue
        witcher.width=50
        super(Swimm,self)._moveIt(witcher)


class Teleport(I_MoveBahavoir):
    def makeMove(self,witcher):
        witcher.color=Colors.Colors.red
        witcher.width=20
        witcher.positionX=witcher.nextPositionX
        witcher.positionY=witcher.nextPositionY
