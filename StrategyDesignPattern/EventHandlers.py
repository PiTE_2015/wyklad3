__author__ = 'ja'

import abc
import pygame
import sys
import Witcher
import MoveBehavoir

class I_EventHandler(object):
     __metaclass__  = abc.ABCMeta

     @abc.abstractmethod
     def handleEvent(self):
         """method for handling the pygame evet"""

class QuitHandler(I_EventHandler):

    def handleEvent(self):
        pygame.quit()
        print "end of the game"
        sys.exit()

class MouseClickHandler(I_EventHandler):
    def __init__(self,witcher):
        self.wither=witcher

    def handleEvent(self):
        positionX,positionY=pygame.mouse.get_pos()
        print ("pressed: x"+str(positionX)+" y"+ str(positionY))
        self.wither.setMove(positionX,positionY)

class KeyDownHandler(I_EventHandler):
    def __init__(self,witcher):
        self.wither=witcher

    def handleEvent(self):
       pressedKeys=pygame.key.get_pressed()
       if pressedKeys[pygame.K_1]:
           self.wither.moveBehavior=MoveBehavoir.Walk()
           print "switch to walk"
       elif pressedKeys[pygame.K_2]:
           self.wither.moveBehavior=MoveBehavoir.Swimm()
           print "switch to swimm"
       elif pressedKeys[pygame.K_3]:
           self.wither.moveBehavior=MoveBehavoir.Teleport()
           print "switch to teleport"


