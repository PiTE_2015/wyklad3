from unittest import TestCase
import Game
import mock
import pygame
import EventHandlers
import sys
__author__ = 'ja'


class TestGame(TestCase):
    def setUp(self):
        self.game =Game.Game()

    @mock.patch('pygame.event.get',return_value=[pygame.event.Event(pygame.QUIT)])
    def test_handleClose(self,mock_getEvent):
        with self.assertRaises(SystemExit):
            self.game.startGame()

    @mock.patch('pygame.event.get',return_value=[pygame.event.Event(pygame.MOUSEBUTTONUP),pygame.event.Event(pygame.QUIT)])
    def test_handleMouseClick(self,mock_getEvent):
        with self.assertRaises(SystemExit):
            self.game.startGame()
