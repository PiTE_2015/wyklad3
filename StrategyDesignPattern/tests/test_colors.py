from unittest import TestCase
import Colors


__author__ = 'ja'


class TestColors(TestCase):

   def setUp(self):
      self.color=Colors.Colors()

   def test_red(self):
      expectedRed= (255, 0, 0)
      self.assertEqual(self.color.red,expectedRed)

   def test_black(self):
      expectedBlack= (0, 0, 0)
      self.assertEqual(self.color.black,expectedBlack)

   def test_white(self):
      expectedWhite= (255, 255, 255)
      self.assertEqual(self.color.white,expectedWhite)
