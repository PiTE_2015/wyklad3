__author__ = 'ja'

import pygame
import math

import Colors
import MoveBehavoir


class Witcher(object):

    def __init__(self, gameDisplay):
        self.positionX=0
        self.positionY=0
        self.speed=10
        self.width=20
        self.height=20
        self.color=Colors.Colors.black
        self.gameDisplay=gameDisplay
        self.nextPositionX=self.positionX
        self.nextPositionY=self.positionY

        self.moveBehavior=MoveBehavoir.Walk()

    def _makeMove(self):
        self.moveBehavior.makeMove(self)

    def setMove(self,positionX,positionY):
        self.nextPositionX=positionX
        self.nextPositionY=positionY


    def draw(self):
        self._makeMove()
        pygame.draw.rect(self.gameDisplay, self.color, [self.positionX, self.positionY, self.width, self.height])


